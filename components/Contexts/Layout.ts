import { createContext, useContext } from 'react';
import { functionThatDoesNothing } from '../../utils/functions';

interface ILayoutContext {
  isSidebarVisibleOnMobile: boolean;
  setIsSidebarVisibleOnMobile: (nextValue: boolean) => void;
}

const layoutContext = createContext<ILayoutContext>({
  isSidebarVisibleOnMobile: false,
  setIsSidebarVisibleOnMobile: functionThatDoesNothing,
});

export const LayoutProvider = layoutContext.Provider;

const useLayout = () => useContext(layoutContext);

export default useLayout;

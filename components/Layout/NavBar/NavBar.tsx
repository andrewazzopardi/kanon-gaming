import { useRouter } from 'next/dist/client/router';
import Link from 'next/link';
import React, { Fragment } from 'react';
import styled from 'styled-components';
import useLayout from '../../Contexts/Layout';
import GridArea from '../../Grid/Area';

interface IContainer {
  isSidebarVisibleOnMobile: boolean;
}

const Container = styled(GridArea) <IContainer>`
  overflow: hidden;
  height: 48px;
  display: grid;
  align-content: center;
  padding: 0 8px 0 ${(props) => (props.isSidebarVisibleOnMobile ? 48 : 208)}px;
  margin-bottom: 8px;
  border-bottom: 1px solid;
  box-shadow: 0 -2px 6px 0 #000;
  grid-auto-flow: column;
  grid-auto-columns: min-content;
  transition: padding 250ms;
  position: sticky;
  top: 0;
  background: white;
  z-index: 1;

  @media (min-width: ${(props) => props.theme.breakpoints.md}) {
    padding-left: 8px;
    width: unset;
  }
`;

const NavBarItem = styled.span`
  text: black;
  padding: 4px;
`;

const NavBarLink = styled(NavBarItem)`
  &:hover {
    text-decoration: underline;
    cursor: pointer;
    background: #ccc;
    border-radius: 8px;
  }
`;

function NavBar() {
  const router = useRouter();
  const { isSidebarVisibleOnMobile } = useLayout();

  const url = router.asPath;
  const backslashes = [];
  for (let i = 0; i < url.length; i += 1) {
    if (url[i] === '/') backslashes.push(i);
  }

  return (
    <Container area="navbar" isSidebarVisibleOnMobile={isSidebarVisibleOnMobile}>
      <Link href="/" passHref>
        <NavBarLink>
          index
        </NavBarLink>
      </Link>
      {backslashes.map((backslash, index) => (
        <Fragment key={backslash}>
          <NavBarItem>/</NavBarItem>
          <Link key={backslash} href={url.substr(0, backslashes[index + 1])} passHref>
            <NavBarLink>
              {url.substr(
                backslash + 1,
                backslashes[index + 1] ? backslashes[index + 1] - 1 : undefined,
              )}
            </NavBarLink>
          </Link>
        </Fragment>
      ))}
    </Container>
  );
}

export default NavBar;

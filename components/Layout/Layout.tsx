import React, { useState } from 'react';
import styled from 'styled-components';
import { LayoutProvider } from '../Contexts/Layout';
import GridArea from '../Grid/Area';
import NavBar from './NavBar/NavBar';
import SideBar from './SideBar/SideBar';

const Container = styled.div`
  display: grid;
  grid-template-rows: 48px 1fr;
  grid-template-columns: 0 1fr;
  grid-template-areas:
    'sidebar navbar'
    'sidebar content';

  @media (min-width: ${(props) => props.theme.breakpoints.md}) {
    grid-template-columns: auto 1fr;
  }
`;

const Content = styled(GridArea)`
  position: relative;
  margin: 8px;
`;

interface ILayoutProps {
  children: React.ReactNode;
}

function Layout({ children }: ILayoutProps) {
  const [isSidebarVisibleOnMobile, setIsSidebarVisibleOnMobile] = useState(false);

  return (
    <LayoutProvider
      value={{
        isSidebarVisibleOnMobile,
        setIsSidebarVisibleOnMobile,
      }}
    >
      <Container>
        <SideBar />
        <NavBar />
        <Content area="content">
          {children}
        </Content>
      </Container>
    </LayoutProvider>
  );
}

export default Layout;

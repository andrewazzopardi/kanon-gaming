import React, { useState } from 'react';
import styled from 'styled-components';
import { ISideBarGroup } from '../../../utils/types';
import { SideBarButton } from './shared';
import SideBarLink from './SideBarLink';

interface IContentProps {
  isOpen: boolean;
}

const Content = styled.div<IContentProps>`
  overflow: hidden;
  max-height: ${(props) => (props.isOpen ? '250px' : 0)};
  transition: max-height 250ms;
`;

interface ISidebarGroupProps {
  group: ISideBarGroup;
}

function SidebarGroup({ group }: ISidebarGroupProps) {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <div>
      <SideBarButton group isOpen={isOpen} onClick={() => setIsOpen((curr) => !curr)}>
        {group.text}
      </SideBarButton>
      <Content isOpen={isOpen}>
        {group.links.map((link) => (
          <SideBarLink key={link.text} link={link} />
        ))}
      </Content>
    </div>
  );
}

export default SidebarGroup;

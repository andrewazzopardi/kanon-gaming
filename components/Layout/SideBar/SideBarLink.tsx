import React from 'react';
import Link from 'next/link';
import { ISideBarLink } from '../../../utils/types';
import { SideBarButton } from './shared';

interface ISideBarLinkProps {
  className?: string;
  link: ISideBarLink;
}

function SideBarLink({ className, link }: ISideBarLinkProps) {
  return (
    <Link href={link.url} passHref>
      <SideBarButton className={className}>
        {link.text}
      </SideBarButton>
    </Link>
  );
}

export default SideBarLink;

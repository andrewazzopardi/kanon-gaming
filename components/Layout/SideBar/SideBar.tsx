import React from 'react';
import styled from 'styled-components';
import tasks from '../../../utils/tasks';
import { isSideBarLink } from '../../../utils/typeGaurds';
import { TSideBarEntry } from '../../../utils/types';
import useLayout from '../../Contexts/Layout';
import GridArea from '../../Grid/Area';
import SidebarGroup from './SideBarGroup';
import SideBarLink from './SideBarLink';

interface IIsVisibleOnMobile {
  isVisibleOnMobile: boolean;
}

const Container = styled(GridArea) <IIsVisibleOnMobile>`
  height: 100vh;
  width: ${(props) => (props.isVisibleOnMobile ? 0 : '200px')};
  box-shadow: -2px 0 6px 0 #000;
  position: sticky;
  top: 0;
  background: white;
  transition: width 250ms;
  z-index: 2;

  @media (min-width: ${(props) => props.theme.breakpoints.md}) {
    width: 200px;
  }
`;

const MenuContent = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  overflow: hidden;
`;

const ToggleMobileSideBar = styled.div<IIsVisibleOnMobile>`
  position: absolute;
  top: 8px;
  left: ${(props) => (props.isVisibleOnMobile ? 8 : 160)}px;
  width: 32px;
  height: 32px;
  background: url('https://static.thenounproject.com/png/878678-200.png');
  background-size: contain;
  transition: left 250ms;
  cursor: pointer;
  z-index: 2;

  @media (min-width: ${(props) => props.theme.breakpoints.md}) {
    display: none;
  }
`;

const BottomSideBarLink = styled(SideBarLink)`
  margin-top: auto;
  border-bottom: none;
  border-top: 1px solid;
`;

const sideBarEntries: TSideBarEntry[] = [
  {
    text: 'Home',
    url: '/',
  },
  {
    text: 'Questions',
    links: tasks.questions.map((question, index) => ({ text: `Question ${index + 1}`, url: `/question/${index + 1}` })),
  },
  {
    text: 'SQL',
    links: tasks.sql.map((_sql, index) => ({ text: `SQL ${index + 1}`, url: `/sql/${index + 1}` })),
  },
];

function SideBar() {
  const { isSidebarVisibleOnMobile, setIsSidebarVisibleOnMobile } = useLayout();

  return (
    <>
      <Container area="sidebar" isVisibleOnMobile={isSidebarVisibleOnMobile}>
        <MenuContent>
          {sideBarEntries.map((sideBarEntry) => (isSideBarLink(sideBarEntry)
            ? <SideBarLink key={sideBarEntry.text} link={sideBarEntry} />
            : <SidebarGroup key={sideBarEntry.text} group={sideBarEntry} />))}
          <BottomSideBarLink link={{ text: 'Source on GitLabs', url: 'https://gitlab.com/andrewazzopardi20/kanon-gaming' }} />
        </MenuContent>
        <ToggleMobileSideBar
          onClick={() => setIsSidebarVisibleOnMobile(!isSidebarVisibleOnMobile)}
          isVisibleOnMobile={isSidebarVisibleOnMobile}
        />
      </Container>
    </>
  );
}

export default SideBar;

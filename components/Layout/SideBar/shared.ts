import styled from 'styled-components';

interface ISideBarButtonProps {
  group?: boolean;
  isOpen?: boolean;
}

export const SideBarButton = styled.div<ISideBarButtonProps>`
  position: relative;
  width: 100%;
  height: 48px;
  padding: 13px;
  border-bottom: 1px solid;
  ${(props) => props.group && `
    background: #aaa;

    &::after {
      content: '▼';
      position: absolute;
      top: 13px;
      right: 13px;
      transform: rotateX(${props.isOpen ? 180 : 0}deg);
      transition: transform 250ms;
    }
  `}

  &:hover {
    cursor: pointer;
    background: #ccc;
  }
`;

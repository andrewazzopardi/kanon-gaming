import styled from 'styled-components';

const TextInput = styled.input`
  height: 32px;
  font-size: 16px;
`;

export default TextInput;

import styled from 'styled-components';

const DefaultButton = styled.button`
  font-size: 16px;
`;

export default DefaultButton;

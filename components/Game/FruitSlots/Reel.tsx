import React from 'react';
import styled, { css, keyframes } from 'styled-components';
import { Fruit, fruitEmoji } from '../../../casino/games/fruitSlots/fruits';
import { useAppSelector } from '../../../redux/hooks';

interface IContainerProps {
  animated: boolean;
  position: number;
  reelId: number;
}

const spin = (reverse: boolean) => keyframes`
  0% {
    transform: translate(0, ${reverse ? -50 : -150}%);
  }

  100% {
    transform: translate(0, ${!reverse ? -50 : -150}%);
  }
`;

const goOnTarget = (reverse: boolean, position: number) => keyframes`
  0% {
    transform: translate(0, ${reverse ? -50 : -150}%);
  }

  100% {
    transform: translate(0, ${-50 - (12.5 * position)}%);
  }
`;

const animationRule = css`
  ${(props: any) => spin(props.reelId === 1)} ${(props: any) => (3 + props.reelId) * 100}ms 0ms 1 ease-in,
  ${(props: any) => spin(props.reelId === 1)} ${(props: any) => (3 + props.reelId) * 100}ms ${(props: any) => ((3 + props.reelId) * 100) * 1}ms 4 linear,
  ${(props: any) => spin(props.reelId === 1)} ${(props: any) => (3 + props.reelId) * 200}ms ${(props: any) => ((3 + props.reelId) * 100) * 5}ms 2 linear,
  ${(props: any) => spin(props.reelId === 1)} ${(props: any) => (3 + props.reelId) * 300}ms ${(props: any) => ((3 + props.reelId) * 100) * 9}ms 1 linear,
  ${(props: any) => goOnTarget(props.reelId === 1, props.position)} ${(props: any) => 300 * props.position}ms ${(props: any) => ((3 + props.reelId) * 100) * 12}ms 1 ease-out forwards
`;

const Container = styled.div<IContainerProps>`
  transform: translate(0, -50%);
  animation: ${(props) => (props.animated ? animationRule : '')};
`;

interface IFruitListProps {
  first?: boolean;
  last?: boolean;
}

const FruitList = styled.div<IFruitListProps>`
  ${(props) => !props.first && 'position: absolute'};
  ${(props) => props.last && 'top: 200%'};
  font-size: 48px;
`;

interface IReelProps {
  reelId: number;
  reel: Fruit[];
  position: number;
}

function Reel({ reelId, reel, position }: IReelProps) {
  const gameAnimated = useAppSelector((state) => state.question4.gameAnimated);

  return (
    <Container reelId={reelId} position={position} animated={gameAnimated}>
      <FruitList first>
        {reel.map((fruit, index) => (
          <div key={index.toString()}>
            {fruitEmoji[Fruit[fruit]]}
          </div>
        ))}
      </FruitList>
      <FruitList>
        {reel.map((fruit, index) => (
          <div key={index.toString()}>
            {fruitEmoji[Fruit[fruit]]}
          </div>
        ))}
      </FruitList>
      <FruitList last>
        {reel.map((fruit, index) => (
          <div key={index.toString()}>
            {fruitEmoji[Fruit[fruit]]}
          </div>
        ))}
      </FruitList>
    </Container>
  );
}

export default Reel;

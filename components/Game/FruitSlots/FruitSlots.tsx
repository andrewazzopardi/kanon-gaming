import React, { useRef } from 'react';
import styled from 'styled-components';
import { Fruit, fruitEmoji } from '../../../casino/games/fruitSlots/fruits';
import { cost } from '../../../casino/games/fruitSlots/logic';
import { reels } from '../../../casino/games/fruitSlots/reels';
import { rewardAndConditions } from '../../../casino/games/fruitSlots/rewards';
import {
  gamePlayError, gamePlayFinished, gamePlayLoading, gamePlayRecieved,
} from '../../../redux/features/question4Slice';
import { useAppDispatch, useAppSelector } from '../../../redux/hooks';
import { backend } from '../../../utils/apis';
import DefaultButton from '../../Input/Button';
import Reel from './Reel';

const Container = styled.div`
`;

const Game = styled.div`
  position: relative;
  display: grid;
  place-items: center;
  grid-auto-flow: column;
  border: 1px solid;
  overflow: hidden;
`;

const Center = styled.div`
  position: absolute;
  top: 50%;
  height: 12.5%;
  width: 100%;
  border: 1px solid;
`;

function FruitSlots() {
  const account = useAppSelector((state) => state.question4.account);
  const gameState = useAppSelector((state) => state.question4.gameState);
  const gameLoading = useAppSelector((state) => state.question4.gameLoading);
  const dispatch = useAppDispatch();
  const timerRef = useRef<any>(null);

  const handlePlay = async () => {
    dispatch(gamePlayLoading());
    try {
      if (timerRef.current) {
        clearTimeout(timerRef.current);
        dispatch(gamePlayFinished(gameState));
      }

      const result = (await backend.post('/casino/games/fruitSlots/play')).data;
      dispatch(gamePlayRecieved(result));

      timerRef.current = setTimeout(() => {
        dispatch(gamePlayFinished(result));
      }, 6000 + (300 * Math.max(...result.positions)));
    } catch {
      dispatch(gamePlayError());
    }
  };

  return (
    <Container>
      <DefaultButton type="button" onClick={handlePlay} disabled={account.coins < cost || gameLoading}>play</DefaultButton>
      <hr />
      <Game>
        {reels.map((reel, index) => (
          <Reel
            key={index.toString()}
            reelId={index}
            reel={reel}
            position={gameState.positions[index]}
          />
        ))}
        <Center />
      </Game>
      <hr />
      {gameState.reward !== null && (
        <div id="lastGameState">
          Last Game:
          {' '}
          {[0, 1, 2].map((index) => fruitEmoji[Fruit[reels[index][gameState.positions[index]]]])}
          <br />
          Coins:
          {' '}
          {gameState.reward}
          <hr />
        </div>
      )}
      <div>
        Winning Info:
        {Object.entries(rewardAndConditions).map(([key, value]) => (
          <div key={key}>
            {`${Array(3).fill(fruitEmoji[Fruit[key]]).join('')} = ${value.rowReward}`}
            <br />
            {`${Array(2).fill(fruitEmoji[Fruit[key]]).join('')} = ${value.pairReward}`}
          </div>
        ))}
      </div>
    </Container>
  );
}

export default FruitSlots;

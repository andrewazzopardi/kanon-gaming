import styled from 'styled-components';

interface IGridAreaProps {
  area: string;
}

const GridArea = styled.div<IGridAreaProps>`
  grid-area: ${(props) => props.area};
`;

export default GridArea;

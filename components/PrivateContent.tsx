import React, { useCallback, useEffect } from 'react';
import styled from 'styled-components';
import { accountLoading, accountRecieved } from '../redux/features/question4Slice';
import { useAppDispatch, useAppSelector } from '../redux/hooks';
import { backend } from '../utils/apis';
import GridArea from './Grid/Area';
import DefaultButton from './Input/Button';

const Coins = styled(GridArea)`
  text-align: right;
`;

interface IPrivateContentProps {
  children: React.ReactNode;
}

function PrivateContent({ children }: IPrivateContentProps) {
  const authLoading = useAppSelector((state) => state.question4.authLoading);
  const account = useAppSelector((state) => state.question4.account);
  const dispatch = useAppDispatch();

  const getAccount = useCallback(() => {
    dispatch(accountLoading());
    backend.get('/casino/account').then((result) => {
      dispatch(accountRecieved(result.data));
    });
  }, [dispatch]);

  useEffect(() => {
    getAccount();
  }, [getAccount]);

  const handleLogin = async () => {
    await backend.post('/casino/login');
    getAccount();
  };

  const handleLogout = async () => {
    await backend.post('/casino/logout');
    getAccount();
  };

  return ((authLoading && <div>loading...</div>) || (account ? (
    <>
      <GridArea area="auth">
        <DefaultButton id="loginButton" type="button" onClick={handleLogout}>logout</DefaultButton>
      </GridArea>
      <Coins area="coins">
        Coins:
        {' '}
        {account.coins}
      </Coins>
      <GridArea area="game">
        <br />
        {children}
      </GridArea>
    </>
  ) : (
    <GridArea area="auth">
      <DefaultButton id="loginButton" type="button" onClick={handleLogin}>login</DefaultButton>
    </GridArea>
  ))
  );
}

export default PrivateContent;

import React from 'react';
import { Provider } from 'react-redux';
import store from '../redux/store';

interface IReduxStoreProps {
  children: React.ReactNode;
}

function ReduxStore({ children }: IReduxStoreProps) {
  return (
    <Provider store={store}>
      {children}
    </Provider>
  );
}

export default ReduxStore;

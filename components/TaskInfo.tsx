import React, { useState } from 'react';
import styled from 'styled-components';

interface IToggleable {
  isActive: boolean;
}

const Container = styled.div<IToggleable>`
  position: fixed;
  box-shadow: 0 0 4px 0 #000;
  border-radius: 16px;
  background: white;
  z-index: 3;
  transition: all 250ms;

  ${(props) => (props.isActive ? `
    bottom: 50%;
    left: 50%;
    transform: translate(-50%, 50%);
    width: 75%;
    padding: 40px 16px 16px;
  ` : `
    bottom: 8px;
    right: 8px;
  `)}
`;

const ToggleButton = styled.div<IToggleable>`
  ${(props) => props.isActive && 'position: absolute'};
  top: 8px;
  right: 8px;
  width: 32px;
  height: 32px;
  background: url('https://static.thenounproject.com/png/${(props) => (props.isActive ? '7294-200' : '93986-200')}.png');
  background-size: contain;
  cursor: pointer;
`;

const Info = styled.div<IToggleable>`
  ${(props) => !props.isActive && 'display: none'};
`;

function TaskInfo({ task }) {
  const [isActive, setIsActive] = useState(false);

  return (
    <Container isActive={isActive}>
      <ToggleButton isActive={isActive} onClick={() => setIsActive(!isActive)} />
      <Info isActive={isActive}>
        {task}
      </Info>
    </Container>
  );
}

export default TaskInfo;

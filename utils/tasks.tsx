/* eslint-disable max-len */
const tasks = {
  questions: [
    <div key={0}>
      Write a function that connects to https://restcountries.eu/ and gets a unique country from a specific name given using the Node back end and send it to the front end.
    </div>,
    <div key={1}>
      Using the same API ( https://restcountries.eu/ ), and from an array of string, write a function that returns a list of countries where their name matches at least a part of one of these string use the Node back end and send it to the front end
    </div>,
    <div key={2}>
      Using the same API ( https://restcountries.eu/ ) in the React front end list all the countries and a field to filter the country by name.
    </div>,
    <pre key={3}>
      Considering a Slot machine defined like this:

      <ul>
        <li>
          Reel1: [“cherry”, ”lemon”, “apple”, ”lemon”, “banana”, “banana”, ”lemon”, ”lemon”]
        </li>
        <li>
          Reel2: [”lemon”, “apple”, ”lemon”, “lemon”, “cherry”, “apple”, ”banana”, ”lemon”]
        </li>
        <li>
          Reel3: [”lemon”, “apple”, ”lemon”, “apple”, “cherry”, “lemon”, ”banana”, ”lemon”]
        </li>
      </ul>

      The user starts with 20 coins. Each spin will cost the user 1 coin.
      Please note that slot machines only consider pairs a match if they are in order from left to right.
      <br />
      Eg:
      <br />
      Apple, Cherry, Apple - no win
      <br />
      Apple, Apple, Cherry - win
      <br />
      <br />
      Rewards
      <ul>
        <li>
          3 cherries in a row: 50 coins, 2 cherries in a row: 40 coins
        </li>
        <li>
          3 Apples in a row: 20 coins, 2 Apples in a row: 10 coins
        </li>
        <li>
          3 Bananas in a row: 15 coins, 2 Bananas in a row: 5 coins
        </li>
        <li>
          3 lemons in a row: 3 coins
        </li>
      </ul>

      Create an endpoint on the backend that when called by the frontend will return the result of the spin and
      the coins the player won
    </pre>,
  ],
  sql: [
    <pre key={0}>
      Use these sentences to draw a schema of a database you would create to store these information.

      <ul>
        <li>You are working in a casino.</li>
        <li>A casino has games.</li>
        <li>Each game has a unique type.</li>
        <li>Each game has one or more countries where players are allowed to bet from.</li>
        <li>A player may or may not have a favorite game.</li>
      </ul>

      Send the image of the schema and also the sql to create the database and tables
    </pre>,
    <div key={1}>
      Write based on above, a SQL query to get all players that have games of type “SLOT” as their favorite games.
    </div>,
  ],
};

export default tasks;

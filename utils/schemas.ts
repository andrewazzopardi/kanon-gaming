import {
  array, number, object, string,
} from 'yup';

export const question1Schema = object({
  text: string().defined(),
}).defined();

export const question2Schema = object({
  filters: array(string()).defined(),
}).defined();

export const authCookieSchema = object({
  coins: number().positive().required(),
}).defined();

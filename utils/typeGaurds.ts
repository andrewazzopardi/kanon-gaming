import { ISideBarLink } from './types';

export const isSideBarLink = (entry: any): entry is ISideBarLink => !!entry.url;

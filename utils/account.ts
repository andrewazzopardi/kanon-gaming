import { NextApiRequest } from 'next';
import { Asserts } from 'yup';
import { authCookieSchema } from './schemas';
import { IAccount } from './types';

interface IAuthCookie extends Asserts<typeof authCookieSchema> { }

export const getAccountFromCookie = (req: NextApiRequest): IAccount => {
  if (req.cookies.auth) {
    try {
      const jsonCookie = JSON.parse(req.cookies.auth);
      const validated: IAuthCookie = authCookieSchema.cast(jsonCookie);
      return validated;
    } catch {
      throw new Error('MalformedAuthCookie');
    }
  }
  return null;
};

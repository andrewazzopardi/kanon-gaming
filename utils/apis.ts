import axios from 'axios';

export const backend = axios.create({
  baseURL: '/api',
});

export const restCountriesClient = axios.create({
  baseURL: 'https://restcountries.eu/rest/v2',
});

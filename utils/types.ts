export interface ISideBarLink {
  text: string;
  url: string;
}

export interface ISideBarGroup {
  text: string;
  links: ISideBarLink[];
}

export type TSideBarEntry = ISideBarGroup | ISideBarLink;

export interface IAccount {
  coins: number;
}

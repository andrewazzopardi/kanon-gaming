import React from 'react';
import { createGlobalStyle, ThemeProvider } from 'styled-components';
import Layout from '../components/Layout/Layout';
import ReduxStore from '../components/ReduxStore';
import theme from '../utils/theme';

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
  }

  * {
    font-family: 'Open Sans', Helvetica, sans-serif;
    box-sizing: border-box;
  }
`;

function MyApp({ Component, pageProps }) {
  return (
    <>
      <GlobalStyle />
      <ReduxStore>
        <ThemeProvider theme={theme}>
          <Layout>
            {/* eslint-disable-next-line react/jsx-props-no-spreading */}
            <Component {...pageProps} />
          </Layout>
        </ThemeProvider>
      </ReduxStore>
    </>
  );
}

export default MyApp;

import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
  display: grid;
  place-content: center;
  height: 100%;
  font-size: 32px;
  text-align: center;

  & > * {
    width: 100%;
  }
`;

function InpexPage() {
  return (
    <Container>
      Kanon Gaming Technical Test
      <hr />
      By Andrew Azzopardi
    </Container>
  );
}

export default InpexPage;

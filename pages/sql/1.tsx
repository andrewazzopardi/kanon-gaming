/* eslint-disable react/no-unescaped-entities */
import React from 'react';
import styled from 'styled-components';
import SQLCode from '../../components/SQLCode';
import TaskInfo from '../../components/TaskInfo';
import tasks from '../../utils/tasks';

const Container = styled.div`
  display: flex;
`;

const Display = styled.img`
  height: 100%;
  border: none;
  flex: 0 0 50%;
`;

const SQLArea = styled.div`
  flex: 0 0 50%;
`;
function SQL1Page() {
  return (
    <Container>
      <SQLArea>
        <SQLCode>
          {`
            CREATE DATABASE Casino;

            CREATE TABLE GameType (
              GameTypeID int NOT NULL PRIMARY KEY,
              Name varchar(30) NOT NULL UNIQUE
            );

            CREATE TABLE Country (
              CountryID int NOT NULL PRIMARY KEY,
              Name varchar(50) NOT NULL UNIQUE
            );

            CREATE TABLE Game (
              GameId int NOT NULL PRIMARY KEY,
              Name varchar(30) NOT NULL UNIQUE
            );

            CREATE TABLE Player (
              PlayerID int NOT NULL PRIMARY KEY,
              Name varchar(30)
            );

            CREATE TABLE FavoriteGames (
              FavourtieID int NOT NULL PRIMARY KEY,
              PlayerID int,
              GameID int,
              FOREIGN KEY(PlayerID) REFERENCES Player(PlayerID),
              FOREIGN KEY(GameID) REFERENCES Game(GameID)
            );

            CREATE TABLE GameAvailableInCountry (
              AvailabilityID int PRIMARY_KEY,
              GameID int,
              CountryID int,
              FOREIGN KEY(GameID) REFERENCES Game(GameID),
              FOREIGN KEY(CountryID) REFERENCES Country(CountryID)
            );
          `}
        </SQLCode>
      </SQLArea>

      <Display
        src="https://mermaid.ink/svg/eyJjb2RlIjoiY2xhc3NEaWFncmFtXG5cblxuY2xhc3MgR2FtZVR5cGUge1xuICAgIEdhbWVUeXBlSUQgaW50LFxuICAgIE5hbWUgc3RyaW5nLFxufVxuXG5jbGFzcyBHYW1lIHtcbiAgICBHYW1lSWQgaW50LFxuICAgIEdhbWVUeXBlSWQgaW50LFxuICAgIE5hbWUgc3RyaW5nLFxufVxuXG5jbGFzcyBHYW1lQXZhaWxhYmxlSW5Db3VudHJpZXMge1xuICAgIEF2YWlsYWJpbGl0eUlkIGludCxcbiAgICBHYW1lSUQgaW50LFxuICAgIENvdW50cnlJRCBpbnRcbn1cblxuY2xhc3MgQ291bnRyeSB7XG4gICAgQ291bnRyeUlEIGludCxcbiAgICBOYW1lIHN0cmluZyxcbn1cblxuY2xhc3MgUGxheWVyIHtcbiAgICBQbGF5ZXJJZCBpbnQsXG4gICAgTmFtZSBzdHJpbmcsXG59XG5cbmNsYXNzIFBsYXllckZhdm91cml0ZXMge1xuICAgIEZhdm91cml0ZUlkIGludCxcbiAgICBQbGF5ZXJJRCBpbnQsXG4gICAgR2FtZUlEIGludCxcbn1cblxuR2FtZVR5cGUgLS18PiBHYW1lXG5cbkNvdW50cnkgLS18PiBHYW1lQXZhaWxhYmxlSW5Db3VudHJpZXNcblxuR2FtZSAtLXw-IEdhbWVBdmFpbGFibGVJbkNvdW50cmllc1xuR2FtZSAtLXw-IFBsYXllckZhdm91cml0ZXNcblxuUGxheWVyIC0tfD4gUGxheWVyRmF2b3VyaXRlcyIsIm1lcm1haWQiOnsidGhlbWUiOiJkZWZhdWx0In0sInVwZGF0ZUVkaXRvciI6dHJ1ZSwiYXV0b1N5bmMiOnRydWUsInVwZGF0ZURpYWdyYW0iOnRydWV9"
      />
      <TaskInfo task={tasks.sql[0]} />
    </Container>
  );
}

export default SQL1Page;

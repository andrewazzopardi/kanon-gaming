/* eslint-disable react/no-unescaped-entities */
import React from 'react';
import SQLCode from '../../components/SQLCode';
import TaskInfo from '../../components/TaskInfo';
import tasks from '../../utils/tasks';

function SQL2Page() {
  return (
    <>
      <SQLCode>
        {`
          SELECT *
          FROM Player
          INNER JOIN FavoriteGames ON FavoriteGames.PlayerID = Player.PlayerID
          INNER JOIN Game ON Game.GameID = FavoriteGames.GameID
          INNER JOIN GameType ON GameType.GameTypeID = Game.GameTypeID
          WHERE GameType.Name = "Slots";
      `}
      </SQLCode>
      <TaskInfo task={tasks.sql[1]} />
    </>
  );
}

export default SQL2Page;

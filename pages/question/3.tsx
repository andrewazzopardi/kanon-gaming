import React, { useEffect } from 'react';
import DefaultButton from '../../components/Input/Button';
import TextInput from '../../components/Input/Text';
import TaskInfo from '../../components/TaskInfo';
import { countriesLoading, countriesRecieved, updateFilter } from '../../redux/features/question3Slice';
import { useAppDispatch, useAppSelector } from '../../redux/hooks';
import { restCountriesClient } from '../../utils/apis';
import tasks from '../../utils/tasks';

function Question3Page() {
  const loading = useAppSelector((state) => state.question3.loading);
  const filter = useAppSelector((state) => state.question3.filter);
  const coutries = useAppSelector((state) => state.question3.countries);
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(countriesLoading());
    restCountriesClient.get('/all').then((result) => {
      dispatch(countriesRecieved(result.data));
    });
  }, [dispatch]);

  const handleUpdateFilter = async () => {
    const text = (document.getElementById('filter') as HTMLInputElement).value;
    dispatch(updateFilter(text));
  };

  return (
    <div>
      <TaskInfo task={tasks.questions[2]} />
      <TextInput id="filter" type="text" />
      <DefaultButton type="button" onClick={handleUpdateFilter}>Update Filter</DefaultButton>
      {loading && 'loading...'}
      {coutries.filter((country) => country.name.toLowerCase().indexOf(filter.toLowerCase()) !== -1)
        .map((country) => (
          <div key={country.name}>{country.name}</div>
        ))}
    </div>
  );
}

export default Question3Page;

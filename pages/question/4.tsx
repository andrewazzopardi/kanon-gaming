import React from 'react';
import styled from 'styled-components';
import FruitSlots from '../../components/Game/FruitSlots/FruitSlots';
import PrivateContent from '../../components/PrivateContent';
import TaskInfo from '../../components/TaskInfo';
import tasks from '../../utils/tasks';

const Container = styled.div`
  display: grid;
  grid-template-areas:
    'auth space coins'
    'game game game';
`;

function Question4Page() {
  return (
    <Container>
      <TaskInfo task={tasks.questions[3]} />
      <PrivateContent>
        <FruitSlots />
      </PrivateContent>
    </Container>
  );
}

export default Question4Page;

import React from 'react';
import styled from 'styled-components';
import { useAppDispatch, useAppSelector } from '../../redux/hooks';
import { countriesLoading, countriesRecieved } from '../../redux/features/question1Slice';
import { backend } from '../../utils/apis';
import TextInput from '../../components/Input/Text';
import DefaultButton from '../../components/Input/Button';
import TaskInfo from '../../components/TaskInfo';
import tasks from '../../utils/tasks';

const Container = styled.div`
  display: grid;
  grid-gap: 8px;
  grid-template-areas:
    'input'
    'button'
    'content';

  @media (min-width: ${(props) => props.theme.breakpoints.md}) {
    grid-template-areas:
      'input button'
      'content content';
    grid-template-columns: 1fr 1fr;
  }
`;

const Content = styled.div``;

function Question1Page() {
  const loading = useAppSelector((state) => state.question1.loading);
  const coutries = useAppSelector((state) => state.question1.countries);
  const dispatch = useAppDispatch();

  const handleSearch = async () => {
    const text = (document.getElementById('input') as HTMLInputElement).value;
    dispatch(countriesLoading());
    const result = await backend.post('/question/1', { text });
    dispatch(countriesRecieved(result.data));
  };

  return (
    <Container>
      <TaskInfo task={tasks.questions[0]} />
      <TextInput id="input" type="text" />
      <DefaultButton type="button" onClick={handleSearch}>Search Country</DefaultButton>
      <Content>
        {loading && 'loading...'}
        {coutries.map((country) => (
          <div key={country.name}>{country.name}</div>
        ))}
      </Content>
    </Container>
  );
}

export default Question1Page;

import React from 'react';
import styled from 'styled-components';
import { useAppDispatch, useAppSelector } from '../../redux/hooks';
import {
  addFilter, countriesLoading, countriesRecieved, removeFilter,
} from '../../redux/features/question2Slice';
import { backend } from '../../utils/apis';
import TextInput from '../../components/Input/Text';
import DefaultButton from '../../components/Input/Button';
import GridArea from '../../components/Grid/Area';
import TaskInfo from '../../components/TaskInfo';
import tasks from '../../utils/tasks';

const Container = styled.div`
  display: grid;
  grid-gap: 8px;
  grid-template-areas:
    'filter addFilter'
    'filters search'
    'space space'
    'content content';
  grid-template-columns: 1fr 1fr;

`;

const FiltersContainer = styled.div`
  display: grid;
  grid-gap: 8px;
  grid-auto-flow: column;
  grid-auto-columns: min-content;
  height: 32px;
`;

const Filter = styled.div`
  display: inline-block;
  padding:  4px 8px;
  background: #ccc;
  border-radius: 16px;
  cursor: pointer;

  &:hover {
    background: #faa;
  }
`;

function Question2Page() {
  const loading = useAppSelector((state) => state.question2.loading);
  const filters = useAppSelector((state) => state.question2.filters);
  const coutries = useAppSelector((state) => state.question2.countries);
  const dispatch = useAppDispatch();

  const handleAddFilter = async () => {
    const text = (document.getElementById('filterInput') as HTMLInputElement).value;
    dispatch(addFilter(text));
  };

  const handleSearch = async () => {
    dispatch(countriesLoading());
    const result = await backend.post('/question/2', { filters });
    dispatch(countriesRecieved(result.data));
  };

  return (
    <Container>
      <TaskInfo task={tasks.questions[1]} />
      <TextInput id="filterInput" type="text" />
      <DefaultButton type="button" onClick={handleAddFilter}>Add Filter</DefaultButton>
      <FiltersContainer>
        {filters.map((filter, index) => (
          <Filter key={filter} onClick={() => dispatch(removeFilter(index))}>{filter}</Filter>
        ))}
      </FiltersContainer>
      <DefaultButton type="button" onClick={handleSearch}>Search Country</DefaultButton>
      <GridArea area="space">
        <hr />
      </GridArea>
      <GridArea area="content">
        {loading && 'loading...'}
        {coutries.map((country) => (
          <div key={country.name}>{country.name}</div>
        ))}
      </GridArea>
    </Container>
  );
}

export default Question2Page;

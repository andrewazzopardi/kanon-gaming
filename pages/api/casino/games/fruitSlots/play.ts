import { NextApiRequest, NextApiResponse } from 'next';
import gameFlow from '../../../../../casino/games/fruitSlots/logic';
import { getAccountFromCookie } from '../../../../../utils/account';
import { setCookie } from '../../../../../utils/cookies';

/**
 * this runs the FruitSlots gameFlow with the logged in account;
 * @throws {NotAuthenticated} if auth cookie is not present or its malformed
 * @throws {FruitSlotsError} if there is any issue while running the game
 */
const handle = (req: NextApiRequest, res: NextApiResponse) => {
  const account = getAccountFromCookie(req);
  if (account) {
    try {
      const gameState = gameFlow(account);
      setCookie(res, 'auth', { coins: account.coins });
      res.send(gameState);
    } catch {
      throw new Error('FruitSlotsError');
    }
  } else {
    throw new Error('NotAuthenticated');
  }
};

export default handle;

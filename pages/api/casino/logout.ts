import { NextApiRequest, NextApiResponse } from 'next';
import { setCookie } from '../../../utils/cookies';

/**
 * this is a logout api to remove the cookie create from the login
 */
const handle = (req: NextApiRequest, res: NextApiResponse) => {
  if (req.method === 'POST') {
    if (!req.cookies.auth) {
      res.statusCode = 401;
    } else {
      setCookie(res, 'auth', null, { path: '/', maxAge: -1 });
    }
  } else {
    res.statusCode = 405;
  }
  res.send(null);
};

export default handle;

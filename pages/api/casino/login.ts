import { NextApiRequest, NextApiResponse } from 'next';
import { setCookie } from '../../../utils/cookies';

const newAccount = { coins: 20 };

/**
 * this is a login api to provide the user with 20 coins
 */

const handle = (req: NextApiRequest, res: NextApiResponse) => {
  if (req.method === 'POST') {
    if (req.cookies.auth) {
      res.statusCode = 401;
    } else {
      setCookie(res, 'auth', newAccount, { path: '/' });
    }
  } else {
    res.statusCode = 405;
  }
  res.send(null);
};

export default handle;

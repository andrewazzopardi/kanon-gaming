import { NextApiRequest, NextApiResponse } from 'next';
import { getAccountFromCookie } from '../../../utils/account';

/**
 * @returns the user's from the auth cookie
 */

const handle = (req: NextApiRequest, res: NextApiResponse) => {
  let response = null;
  if (req.method === 'GET') {
    response = getAccountFromCookie(req);
  } else {
    res.statusCode = 405;
  }
  res.send(response);
};

export default handle;

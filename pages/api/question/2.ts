import { NextApiRequest, NextApiResponse } from 'next';
import { Asserts } from 'yup';
import { restCountriesClient } from '../../../utils/apis';
import { question2Schema } from '../../../utils/schemas';

interface Question2Input extends Asserts<typeof question2Schema> { }

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const { body } = req;
  let response: object;

  try {
    const validated: Question2Input = question2Schema.cast(body);
    try {
      const allResults = await Promise.all(validated.filters.map((filter) => restCountriesClient.get(`/name/${filter}?fields=name`)));
      response = allResults.reduce((array, curr) => {
        array.push(...curr.data);
        return array;
      }, []);
    } catch {
      response = [{ name: 'No countries found' }];
    }
  } catch {
    response = null;
    res.statusCode = 400;
  }
  res.send(response);
};

export default handler;

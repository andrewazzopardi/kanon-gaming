import { NextApiRequest, NextApiResponse } from 'next';
import { Asserts } from 'yup';
import { restCountriesClient } from '../../../utils/apis';
import { question1Schema } from '../../../utils/schemas';

interface Question1Input extends Asserts<typeof question1Schema> { }

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const { body } = req;
  let response: object;

  try {
    const validated: Question1Input = question1Schema.cast(body);
    try {
      const result = await restCountriesClient.get(`/name/${validated.text}?fields=name&fullText=true`);
      response = result.data;
    } catch {
      response = [{ name: 'Countries not found' }];
    }
  } catch {
    response = null;
    res.statusCode = 400;
  }
  res.send(response);
};

export default handler;

# Kanon Gaming

## Usage

This application is already available at https://andrew-azzopardi-kanon-gaming-andrewazzopardi20.vercel.app/

## Installation

- `npm install`
- `npm run dev`

## Testing - Requires Installation

test coverage is also available at https://andrewazzopardi20.gitlab.io/kanon-gaming/

- `npm run test`
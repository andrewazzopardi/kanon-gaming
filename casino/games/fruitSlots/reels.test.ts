import { reels, spinAllReels } from './reels';

describe('FruitSlots Reels', () => {
  it('should get 3 fruits with their correct position on the reels', () => {
    const spunReels = spinAllReels();
    expect(spunReels.map((reel) => reel.fruit))
      .toEqual(reels.map((reel, index) => reel[spunReels[index].position]));
  });
});

import { IAccount } from '../../../utils/types';
import { spinAllReels } from './reels';
import { getRewardFromFruits } from './rewards';

export const cost = 1;

/**
 * Starts to flow of the Game: FruitSlots
 * @param account the account which the cost and rewards effect
 * @returns returns the end gameState
 */

const gameFlow = (account: IAccount) => {
  if (account.coins >= cost) {
    account.coins -= 1;
    const reels = spinAllReels();
    const reward = getRewardFromFruits(reels.map((reel) => reel.fruit));
    account.coins += reward;
    return { reward, positions: reels.map((reel) => reel.position) };
  }
  throw new Error('NotEnoughFunds');
};

export default gameFlow;

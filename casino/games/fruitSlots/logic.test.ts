import gameFlow from './logic';

describe('FruitSlots gameFlow', () => {
  it('should not run if account has insufficient funds', () => {
    expect(() => gameFlow({ coins: 0 })).toThrow('NotEnoughFunds');
  });

  it('should return rewards and reel positions if account has sufficient funds', () => {
    const { reward, positions } = gameFlow({ coins: 1 });
    expect(reward).toBeDefined();
    expect(positions.length).toBe(3);
  });
});

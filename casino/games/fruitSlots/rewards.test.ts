import { Fruit } from './fruits';
import { getRewardFromFruits, rewardAndConditions } from './rewards';

const winningFruit = Fruit.apple;
const losingFruit = Fruit.cherry;

describe('FruitSlots Rewards', () => {
  it('should give rowRewards on 3 fruits', () => {
    const fruits = [winningFruit, winningFruit, winningFruit];
    const reward = getRewardFromFruits(fruits);
    expect(reward).toBe(rewardAndConditions[winningFruit].rowReward);
  });

  it('should give pairRewards on 2 fruits on left side', () => {
    const fruits = [winningFruit, winningFruit, losingFruit];
    const reward = getRewardFromFruits(fruits);
    expect(reward).toBe(rewardAndConditions[winningFruit].pairReward);
  });

  it('should give pairRewards on 2 fruits on right side', () => {
    const fruits = [losingFruit, winningFruit, winningFruit];
    const reward = getRewardFromFruits(fruits);
    expect(reward).toBe(rewardAndConditions[winningFruit].pairReward);
  });

  it('should not give pairRewards on 2 fruits not touching', () => {
    const fruits = [winningFruit, losingFruit, winningFruit];
    const reward = getRewardFromFruits(fruits);
    expect(reward).toBe(0);
  });

  it('should throw error if not an array of 3 fruit', () => {
    const fruits = [winningFruit, winningFruit];
    const func = () => getRewardFromFruits(fruits);
    expect(func).toThrow();
  });
});

import { Fruit } from './fruits';

export const reels = [[
  Fruit.cherry,
  Fruit.lemon,
  Fruit.apple,
  Fruit.lemon,
  Fruit.banana,
  Fruit.banana,
  Fruit.lemon,
  Fruit.lemon,
], [
  Fruit.lemon,
  Fruit.apple,
  Fruit.lemon,
  Fruit.lemon,
  Fruit.cherry,
  Fruit.apple,
  Fruit.banana,
  Fruit.lemon,
], [
  Fruit.lemon,
  Fruit.apple,
  Fruit.lemon,
  Fruit.apple,
  Fruit.cherry,
  Fruit.lemon,
  Fruit.banana,
  Fruit.lemon,
]];

interface IReelState {
  fruit: Fruit;
  position: number;
}

/**
 * gets a random position from the {@link reel}
 * and returns the state of that {@link position}
 * @param reel an array of Fruit
 * @returns {IReelState} reelState
 */
const spinReel = (reel: Fruit[]): IReelState => {
  const position = Math.floor(Math.random() * reel.length);
  return { fruit: reel[position], position };
};

/**
 * spins all 3 reels and returns and array of the result
 */
export const spinAllReels = () => reels.map(spinReel);

import { Fruit } from './fruits';

interface IRewardAndCondition {
  rowReward: number,
  pairReward: number,
}

/**
 * @typedef Fruit
 * the rewards for every {@link Fruit}
 */
export const rewardAndConditions: Record<Fruit, IRewardAndCondition> = {
  [Fruit.cherry]: {
    rowReward: 50,
    pairReward: 40,
  },
  [Fruit.apple]: {
    rowReward: 20,
    pairReward: 10,
  },
  [Fruit.banana]: {
    rowReward: 15,
    pairReward: 5,
  },
  [Fruit.lemon]: {
    rowReward: 3,
    pairReward: 0,
  },
};

/**
 * checks if {@link fruits}[0] and {@link fruits}[2] match with {@link fruits}[1]
 * if both match it returns a row reward
 * if only one match it returns a pair reward
 * else it returns no reward
 * rewards are from {@link rewardAndConditions}
 * @param fruits array of 3 {@link Fruit}
 * @returns a reward according to the input array
 * @throws if the array is not of 3 {@link Fruit}
 */

export const getRewardFromFruits = (fruits: Fruit[]) => {
  if (fruits.length === 3) {
    const baseFruit = fruits[1];
    const firstPairMatch = fruits[0] === baseFruit;
    const secondPairMatch = baseFruit === fruits[2];

    if (firstPairMatch && secondPairMatch) {
      return rewardAndConditions[baseFruit].rowReward;
    } if (firstPairMatch || secondPairMatch) {
      return rewardAndConditions[baseFruit].pairReward;
    }
    return 0;
  }
  throw new Error('GamePlayError');
};

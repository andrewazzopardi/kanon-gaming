export enum Fruit {
  apple,
  cherry,
  lemon,
  banana,
}

export const fruitEmoji = {
  apple: '🍎',
  cherry: '🍒',
  lemon: '🍋',
  banana: '🍌',
};

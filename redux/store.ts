import { configureStore } from '@reduxjs/toolkit';
import question1Reducer from './features/question1Slice';
import question2Reducer from './features/question2Slice';
import question3Reducer from './features/question3Slice';
import question4Reducer from './features/question4Slice';

const store = configureStore({
  reducer: {
    question1: question1Reducer,
    question2: question2Reducer,
    question3: question3Reducer,
    question4: question4Reducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export default store;

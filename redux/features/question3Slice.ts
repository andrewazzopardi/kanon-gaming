import { createSlice } from '@reduxjs/toolkit';

export const question3Slice = createSlice({
  name: 'question3',
  initialState: {
    filter: '',
    loading: false,
    countries: [],
  },
  reducers: {
    updateFilter: (state, action) => {
      state.filter = action.payload;
    },
    countriesLoading: (state) => {
      state.loading = true;
    },
    countriesRecieved: (state, action) => {
      state.countries = action.payload;
      state.loading = false;
    },
  },
});

export const {
  updateFilter, countriesLoading, countriesRecieved,
} = question3Slice.actions;

export default question3Slice.reducer;

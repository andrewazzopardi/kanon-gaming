import { createSlice } from '@reduxjs/toolkit';

export const question1Slice = createSlice({
  name: 'question1',
  initialState: {
    loading: false,
    countries: [],
  },
  reducers: {
    countriesLoading: (state) => {
      state.loading = true;
    },
    countriesRecieved: (state, action) => {
      state.countries = action.payload;
      state.loading = false;
    },
  },
});

export const { countriesLoading, countriesRecieved } = question1Slice.actions;

export default question1Slice.reducer;

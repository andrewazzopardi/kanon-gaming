import { createSlice } from '@reduxjs/toolkit';

const initialGameState = { positions: [0, 0, 0], reward: null };

export const question4Slice = createSlice({
  name: 'question4',
  initialState: {
    authLoading: false,
    account: null,
    gameLoading: false,
    gameState: initialGameState,
    gameAnimated: false,
  },
  reducers: {
    accountLoading: (state) => {
      state.authLoading = true;
    },
    accountRecieved: (state, action) => {
      state.account = action.payload;
      state.gameAnimated = false;
      state.authLoading = false;
    },
    gamePlayLoading: (state) => {
      state.account = { ...state.account, coins: state.account.coins - 1 };
      state.gameLoading = true;
      state.gameAnimated = false;
      state.gameState = initialGameState;
    },
    gamePlayRecieved: (state, action) => {
      if (state.account) {
        state.gameState = action.payload;
        state.gameLoading = false;
        state.gameAnimated = true;
      } else {
        throw new Error('Not authenticated');
      }
    },
    gamePlayFinished: (state, action) => {
      if (state.account) {
        state.account = { ...state.account, coins: state.account.coins + action.payload.reward };
      } else {
        throw new Error('Not authenticated');
      }
    },
    gamePlayError: (state) => {
      if (state.account) {
        state.account = { ...state.account, coins: state.account.coins + 1 };
        state.gameLoading = false;
        state.gameAnimated = false;
        state.gameState = initialGameState;
      } else {
        throw new Error('Not authenticated');
      }
    },
  },
});

export const {
  accountLoading, accountRecieved, gamePlayLoading, gamePlayRecieved, gamePlayError,
  gamePlayFinished,
} = question4Slice.actions;

export default question4Slice.reducer;

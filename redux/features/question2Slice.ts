import { createSlice } from '@reduxjs/toolkit';

export const question2Slice = createSlice({
  name: 'question2',
  initialState: {
    filters: [],
    loading: false,
    countries: [],
  },
  reducers: {
    addFilter: (state, action) => {
      if (state.filters.indexOf(action.payload) === -1) {
        state.filters.push(action.payload);
      }
    },
    removeFilter: (state, action) => {
      state.filters.splice(action.payload, 1);
    },
    countriesLoading: (state) => {
      state.loading = true;
    },
    countriesRecieved: (state, action) => {
      state.countries = action.payload;
      state.loading = false;
    },
  },
});

export const {
  addFilter, removeFilter, countriesLoading, countriesRecieved,
} = question2Slice.actions;

export default question2Slice.reducer;

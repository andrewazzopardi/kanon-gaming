module.exports = {
  preset: 'ts-jest',
  setupFilesAfterEnv: ['<rootDir>testUtils/setupEnzyme.ts'],
  collectCoverageFrom: ['**/*.ts', '!**/*.d.ts', '!**/node_modules/**'],
  coverageReporters: ['html', 'text', 'text-summary', 'cobertura'],
  testMatch: ['**/*.test.(ts|tsx)'],
  testPathIgnorePatterns: [
    './.next/',
    './node_modules/',
  ],
};
